//Fundamentals Part 3

// For now, just write each function and test the output with console.log.

// Write a function called add7 that takes one number and returns that number + 7.
// Write a function called multiply that takes 2 numbers and returns their product.
// Write a function called capitalize that takes a string and returns that string with only the first letter capitalized. Make sure that it can take strings that are lowercase, UPPERCASE or BoTh.
// Write a function called lastLetter that takes a string and returns the very last letter of that string:
// lastLetter("abcd") should return "d"

function add7(num){
    return num+7;
}

console.log(add7(13),add7(21),add7(26));

function multiply(num1,num2){
    return num1*num2
}

console.log(multiply(7,10), multiply(13,2), multiply(8,6))

function capitalize(string){
 string.toLowerCase();
 let initialLUppercasedLetter = string.slice(0,1).toUpperCase();
 let remainingLetters = string.slice(1,string.length).toLowerCase();
 return initialLUppercasedLetter.concat(remainingLetters)
}

console.log(capitalize('lyca'), capitalize('sCoTt'), capitalize('TEST'))

function lastLetter(string){
    return string.slice(string.length-1)
}

console.log(lastLetter('abcde'));